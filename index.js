
module.exports = class {
  constructor(table, categ_table, cmbird) {
    let app = this.app = cmbird.app;
    const path_prefix = "/content-manager";

    this.table = table;
    this.categ_table = categ_table;

    let this_class = this;

    app.get(path_prefix+"/posts", async function(req, res) {
      try {
        var data = JSON.parse(req.query.data);
        /*
          {
            command: string -> "all"|"select"|"select_by_title",
            title: "string",
            tags: ["string"]
          }
        */
        switch (data.command) {
          case 'all':
            var list = await this_class.all();
            list.sort(function (a, b) {
              return a.publish_date - b.publish_date;
            });
            res.send(JSON.stringify(list));
            break;
          case 'page':
            let alist = await this_class.all(data.category);
            alist.sort(function (a, b) {
              return b.publish_date - a.publish_date;
            });
            let real_list = [];
            for (let i = data.min; i < data.max; i++) {
              if (alist[i]) {
                real_list.push(alist[i]);
              }
            }

            real_list.sort(function (a, b) {
              return a.publish_date - b.publish_date;
            });

            res.send(JSON.stringify(real_list));
            break;
          case 'max_index':
            let maxi_qstr = 'select count(*) AS exact_count FROM posts';
            if (data.category) maxi_qstr += " WHERE categories @> ARRAY['"+data.category+"']::text[]";
            const max_indexa = (await table.query(maxi_qstr)).rows[0].exact_count;
            res.json(parseInt(max_indexa));
            break;
          case 'select':
            if (data.title) {
              var result = await this_class.select_by_title(data.title);
              result.sort(function (a, b) {
                return a.publish_date - b.publish_date;
              });
              res.send(JSON.stringify(result));
            } else if (data.tags) {
              var result = await this_class.select_by_tags(data.tags);
              result.sort(function (a, b) {
                return a.publish_date - b.publish_date;
              });
              res.send(JSON.stringify(result));
            }
            break;
          case 'all_categories':
            var list = await this_class.all_categories();
            res.json(list);
            break;
          default:
            console.log("Content Manager (GET): unknown command", data.command);
        }
      } catch(e) {
        console.error(e.stack);
      };
    });

    app.post(path_prefix+"/posts", cmbird.admin.auth.orize_gen(["content_management"]), async function(req, res) {
      try {
        var data = JSON.parse(req.body.data);
        /*
          {
            command: string -> "create"|"edit"|"delete",
            title: "string",
            post: {
              title: "string",
              tags: ["string"],
              content: "string"
            },
            ids: ["UUID"]
          }
        */
        switch (data.command) {
          case 'create':
            if (data.post) {
              var json = await this_class.create(data.post);
              res.send(json);
            }
            break;
          case 'edit':
            if (data.post) {
              var json = await this_class.edit(data.post);
              res.send(json);
            }
            break;
          case 'delete':
            if (data.ids) {
              this_class.delete(data.ids);
              res.send("success");
            }
            break;
          case 'add_category':
            if (data.name) {
              var json = await this_class.add_category(data.name);
              res.send(json);
            }
            break;
          case 'del_category':
            if (data.name) {
              var json = await this_class.del_category(data.name);
              res.send(json);
            }
            break;
          default:
            console.log("Content Manager (POST): unknown command", data.command);
        }
      } catch(e) {
        console.error(e.stack);
      };
    });

  }

  static async init(cmbird) {
    try {
      console.log("+", "Posts");
      let table = await cmbird.aura.table('posts', {
        columns: {
          id: 'uuid',
          title: 'text',
          content: 'text',
          tags: 'text[]',
          categories: 'text[]',
          publish_date: 'bigint',
          edit_date: 'bigint'
        }
      });

      let categ_table = await cmbird.aura.table('post_categories', {
        columns: {
          id: 'uuid',
          name: 'text'
        }
      });

      return new module.exports(table, categ_table, cmbird);
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

// -- GET
  async all(categ) {
    try {
      let posts = false;
      if (categ) {
        posts = await this.table.select('*', 'categories @> $1', [[categ]]);
      } else {
        posts = await this.table.select('*');
      }
      return posts;
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async all_categories() {
    try {
      var posts = await this.categ_table.select('*');
      return posts;
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async select_by_tags(tags) {
    try {
      var found = await this.table.select(
        "*", "tags && $1", [tags]
      );
      return found;
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  };

  async select_by_title(title) {
    try {
      return await this.table.select(
        '*', "title = $1", [title]
      );
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

// -- POST
  async create(data) {
    try {
      var found = await this.select_by_title(data.title);

      var json;
      if (found.length == 0) {
        data.publish_date = Date.now();
        var result = await this.table.insert(data);
        json = JSON.stringify({
          id: result
        });
      } else {
        json = JSON.stringify({
          err: "Post named `"+data.title+"` already exists!"
        });
      }
      return json;
    } catch (e) {
      console.error(e.stack);
      return false;
    }

  }

  async add_category(name) {
    try {
      var found = await this.categ_table.select("*", "name = $1", [name]);

      var json;
      if (found.length == 0) {
        var result = await this.categ_table.insert({
          name: name
        });
        json = JSON.stringify({
          id: result
        });
      } else {
        json = JSON.stringify({
          err: "Category named `"+name+"` already exists!"
        });
      }
      return json;
    } catch (e) {
      console.error(e.stack);
      return false;
    }

  }

  
  async del_category(name) {
    try {
      await this.categ_table.delete("name = $1", [name]);
      return {};
    } catch (e) {
      console.error(e.stack);
      return false;
    }

  }


  async edit(data) {
    try {
      var id = data.id;
      delete data.id;
      data.edit_date = Date.now();
      await this.table.update(data, "id = $1", [id]);
      return "success";
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async delete(ids) {
    try {
      if (ids) {
        var where = '';
        for (var i = 0; i < ids.length; i++) {
          if (i > 0) {
            where += ' OR id = $'+(i+1);
          } else {
            where += 'id = $'+(i+1);
          }
        }
        await this.table.delete(where, ids);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }
}
